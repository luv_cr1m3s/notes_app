import asyncio
from asgiref.sync import sync_to_async
from django.shortcuts import HttpResponse, render, redirect, get_object_or_404
from django.views import View
from django.contrib.auth import login
from django.urls import reverse
from notes.forms import CustomUserCreationForm
from .models import Note, Category
from .forms import NoteForm


class IndexView(View):
    
#    @sync_to_async
    def get(self, request):
        category_title = request.GET.get('category', '')
        categories = Category.objects.all()

        if category_title:
            notes = self.get_filtered_notes(category_title) 
        else:
            notes =self.get_all_notes() 
        username = None 
        if request.user.is_authenticated:
            username = request.user
        #    notes = notes.filter(creator = request.user)
        #else:
        #    notes = notes.filter(creator = None)

        form = NoteForm()
        form.fields['creator'].disabled=True
        return render(request, 'index.html', {'notes': notes, 'categories': categories, 'form': form, 'username': username})
    
#    @sync_to_async
    def post(self, request):
        form = NoteForm(request.POST, initial={'creator':request.user})
        form.fields['creator'].disabled = True
        if form.is_valid():
            form.save()
            return redirect('index')
        else:
            return HttpResponse(f"something wrong with: {form.errors}")

    def get_all_notes(self):
        return Note.objects.all()

    def get_filtered_notes(self, category):
        return Note.objects.filter(category__title=category)

def note_detail(request, note_id):
    note = get_object_or_404(Note, pk=note_id)
    if request.method == 'POST':
        form = NoteForm(request.POST, instance = note)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = NoteForm(instance=note)

    return render(request, 'note_detail.html', {'form': form, 'note': note}) 


def delete_note(request, note_id):
    note = get_object_or_404(Note, pk=note_id)
    if request.method == 'POST':
        if 'confirm' in request.POST:
            note.delete()
        return redirect('index')
    return render(request, 'delete_note.html', {'note': note})


def dashboard(request):
    return render(request, "dashboard.html")


def register(request):

    if request.method == "GET":
        return render(
            request, "register.html",
            {"form": CustomUserCreationForm}
        )
    elif request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(reverse("dashboard"))
        else:
            return HttpResponse(f"Something is wrong with: {form.errors}")
