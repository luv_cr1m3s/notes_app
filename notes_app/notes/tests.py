from django.test import TestCase, Client
from django.urls import reverse
from .models import Note, Category
from .forms import NoteForm

class IndexViewTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_index_view_get(self):
        url = reverse('index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertIsInstance(response.context['form'], NoteForm)

    def test_index_view_post_valid_form(self):
        category = Category.objects.create(title='Test Category')
        url = reverse('index')
        form_data = {'title': 'Test Note', 'text': 'Testing', 'reminder': '2023-07-01', 'category': category.id}
        response = self.client.post(url, data=form_data)
        self.assertEqual(response.status_code, 302)  # Redirect status code
        self.assertRedirects(response, reverse('index'))  # Check redirection to index

    def test_index_view_post_invalid_form(self):
        category = Category.objects.create(title='Test Category')
        url = reverse('index')
        form_data = {'title': '', 'text': 'Testing', 'reminder': '2023-07-01', 'category': category.id}
        response = self.client.post(url, data=form_data)
        form = NoteForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(response.status_code, 200)
        self.assertFormError(form, 'title', 'This field is required.')

class NoteFormTest(TestCase):
    def test_note_form_valid(self):
        category = Category.objects.create(title='Test Category')
        form_data = {'title': 'Test Note', 'text': 'Testing', 'reminder': '2023-07-01', 'category': category.id}
        form = NoteForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_note_form_invalid(self):
        category = Category.objects.create(title='Test Category')
        form_data = {'title': '', 'text': 'Testing', 'reminder': '2023-07-01', 'category': category.id}
        form = NoteForm(data=form_data)
        self.assertFalse(form.is_valid())
