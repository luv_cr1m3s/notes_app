import os
import datetime
from random import randint

os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'notes_app.settings')

import django
django.setup()
from notes.models import Category, Note


def add_category(name):
    c = Category.objects.get_or_create(title=name)[0]

    c.save()
    return c


def add_note(category, title, text, reminder):

    n = Note.objects.get_or_create(category = category)[0]
    n.title = title
    n.text = text
    n.reminder = reminder

    n.save()
    return n

def rand_date():
    return datetime.date(randint(2005,2025), randint(1,12),randint(1,28))


def populate():

    categories = ["History", "Art", "Math", "Chemistry", "CompSci"]

    text = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
              sed do eiusmod tempor incididunt ut labore et dolore magna
              aliqua. Ut enim ad minim veniam, quis nostrud exercitation
              ullamco laboris nisi ut aliquip ex ea commodo consequat. 
              Duis aute irure dolor in reprehenderit in voluptate velit
              esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
              occaecat cupidatat non proident, sunt in culpa qui officia
              deserunt mollit anim id est laborum."""
   
    title = [f"{i} title"  for i in categories]
    reminder = [rand_date() for _ in range(len(title))]

    for i in range(0, len(categories)):
        cat = add_category(categories[i])
        add_note(cat, title[i], text, reminder[i])

if __name__ == '__main__':
    populate()
