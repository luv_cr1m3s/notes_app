import httpx
import asyncio
from time import perf_counter

async def async_req(url):
    result = []
    async with httpx.AsyncClient() as client:
        result = await client.get(url)

    return result

async def main():
    t1 = perf_counter()

    url = "http://localhost:8000"
    time_list = []
    
    for i in range(1, 100):
        time_list.append(async_req(url))

    res = await asyncio.gather(*time_list)

    print(res)

    t2 = perf_counter()

    print(f"100 calls took: {t2-t1} seconds")

if __name__ == '__main__':
    asyncio.run(main())

